/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuyou.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.fuyou.FuyouConstants;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouSignTypeEnums;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class FuyouApiMessage implements ApiMessage {

    @XStreamOmitField
    private String service;

    @FuyouAlias("MCHNTCD")
    @XStreamAlias("MCHNTCD")
    private String partner;

    @XStreamAlias(value = "VERSION")
    @FuyouAlias(value = "VERSION")
    private String version;

    @XStreamAlias("TYPE")
    private String serviceType;

    @XStreamAlias("SIGN")
    private String sign;

    @XStreamAlias("SIGNTP")
    private String signType = FuyouSignTypeEnums.MD5.getKey();

    @XStreamOmitField
    private String gatewayUrl;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String getSignStr() {
        return null;
    }

    public void doCheck() {

    }

}

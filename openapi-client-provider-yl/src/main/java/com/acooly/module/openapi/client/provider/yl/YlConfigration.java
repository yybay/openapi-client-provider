package com.acooly.module.openapi.client.provider.yl;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;

import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.yl.YlProperties.PREFIX;


@EnableConfigurationProperties({YlProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class YlConfigration {

    @Autowired
    private YlProperties ylProperties;

    @Bean("ylHttpTransport")
    public Transport weBankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(ylProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(ylProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(ylProperties.getReadTimeout()));
        httpTransport.setContentType(ContentType.TEXT_PLAIN.getMimeType());
        httpTransport.setCharset("GBK");
        return httpTransport;
    }
}

package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 19:02
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.DEDUCT,type = ApiMessageType.Request)
public class YipayDeductRequest extends YipayRequest {

    /**
     * 外部订单号
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "extOrderSeq")
    private String tradeOrderNo;

    /**
     * 交易金额
     * 只能为小于18位的非零正整数，以分为单位，只支持人民币
     */
    @NotBlank
    @Size(max = 17)
    @YipayAlias(value = "tradeAmount")
    private String amount;

    /**
     * 银行卡号
     * 付款银行卡号
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "bankCardNo")
    private String bankCardNo;

    /**
     * 银行卡户名
     * 付款银行卡账户户名
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "acctName")
    private String realName;

    /**
     * 银行编码
     *参考com.acooly.module.openapi.client.provider.yipay.enums.YipayBankCodeEnum
     */
    @NotBlank
    @Size(max = 6)
    @YipayAlias(value = "bankCode")
    private String bankCode;

    /**
     * 银行卡卡类型
     * 1 :借记卡
     * 2:信用卡(贷记卡)
     * 4:存折
     * 8:公司账户
     */
    @NotBlank
    @Size(max = 1)
    @YipayAlias(value = "cardType")
    private String cardType = "1";

    /**
     * 银行卡户名
     * 0：对公
     * 1：对私
     */
    @NotBlank
    @Size(max = 1)
    @YipayAlias(value = "perEntFlag")
    private String perEntFlag = "1";

    /**
     * 银行区域码
     * 參考com.acooly.module.openapi.client.provider.yipay.enums.YipayBankAreaCodeEnum
     * 如果取不到可以就用这个默认值
     */
    @NotBlank
    @Size(max = 6)
    @YipayAlias(value = "areaCode")
    private String areaCode = "110000";

    /**
     * 证件号
     * 银行卡开户证件号
     * 当证件类型为00身份证时需做格式校验
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "certNo")
    private String certNo;

    /**
     * 证件号
     * 00 身份证
     * 01 护照
     * 02 军人证
     * 03 户口簿
     * 04 武警证
     * 05 法人营业执照
     * 06 港澳通行证
     * 07 台湾通行证
     * 08 学生证
     * 09 工作证
     * 10 工商执照
     * 11 警官证
     * 12 事业单位编码
     * 13 房产证
     * 51 组织机构编码
     * 99 其它证件
     */
    @NotBlank
    @Size(max = 2)
    @YipayAlias(value = "certType")
    private String certType = "00";

    /**
     * 开户支行名
     * 银行卡开户支行名
     * 【请尽量填写】
     */
    @Size(max = 64)
    @YipayAlias(value = "branchName")
    private String branchName;

    /**
     * 联系方式 手机号
     * 银行卡开户联系方式
     */
    @NotBlank
    @Size(max = 11)
    @YipayAlias(value = "mobile")
    private String mobile;

    /**
     * 外部用户标识
     * 由商户提供，一般只是作为查询条件，相当于外部系统的保留或者关联字段
     */
    @Size(max = 32)
    @YipayAlias(value = "extUserId")
    private String extUserId;

    /**
     * 交易摘要
     * 填写购买商品的名称
     */
    @NotBlank
    @Size(max = 64)
    @YipayAlias(value = "trsSummary")
    private String trsSummary;

    /**
     * 交易备注
     * 金额大于50000元则必填
     */
    @Size(max = 128)
    @YipayAlias(value = "trsMemo")
    private String trsMemo;

    /**
     * 二级商户号
     * 门户报备二级商户所得的二级商户号
     */
    @Size(max = 128)
    @YipayAlias(value = "secCustCode")
    private String secCustCode;

}

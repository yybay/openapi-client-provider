package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.WHOLE_NETWORK_MOBILE_TIME_QUERY,type = ApiMessageType.Response)
public class YipayWholeNetworkMobileTimeQueryResponse extends YipayResponse {

    /**
     * 结果标识
     * 03:0-3个月
     * 06:3-6个月
     * 12:6-12个月
     * 24:12-24个月
     * 99:24个月以上
     */
    private String stat;

    /**
     * 结果描述
     */
    private String desc;
}

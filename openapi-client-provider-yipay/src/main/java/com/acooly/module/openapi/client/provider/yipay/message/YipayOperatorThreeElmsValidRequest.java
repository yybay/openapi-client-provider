package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.OPERATOR_THREE_ELMS_VALID,type = ApiMessageType.Request)
public class YipayOperatorThreeElmsValidRequest extends YipayRequest {

    /**
     * 姓名
     */
    @NotBlank
    @YipayAlias(value = "name")
    private String realName;

    /**
     *  身份证号
     */
    @NotBlank
    @YipayAlias(value = "certNo")
    private String certNo;

    /**
     * 手机号
     */
    @YipayAlias(value = "mobile")
    private String mobile;
}

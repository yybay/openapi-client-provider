package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 20:22
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.TRADE_QUERY,type = ApiMessageType.Request)
public class YipayTradeQueryRequest extends YipayRequest {

    /**
     * 外部订单号
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "originReqSeq")
    private String tradeOrderNo;

    /**
     * 订单类型
     * TRADE 交易类
     * REFUND 退款类
     * 不传则默认为TRADE交易类
     */
    @Size(max = 6)
    @YipayAlias(value = "orderType")
    private String orderType;

}

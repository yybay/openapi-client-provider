/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.yipay.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu 2018-01-23 12:58
 * <p>
 * 如果是响应错误，则content都是固定的格式:
 * "merchantNo": "M0980033473",
 * "orderDate": "20171025",
 * "orderNo": "20171025000004923708"
 */
@Getter
@Setter
public class YipayResponse extends YipayMessage {

    private String success;

    private String retCode;

    private String retMsg;

}

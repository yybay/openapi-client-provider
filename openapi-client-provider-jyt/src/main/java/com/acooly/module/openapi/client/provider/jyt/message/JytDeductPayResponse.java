package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytDeductPayResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/8 15:46
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.DEDUCT_PAY,type = ApiMessageType.Response)
@XStreamAlias("message")
public class JytDeductPayResponse extends JytResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private JytDeductPayResponseBody deductPayResponseBody;
}

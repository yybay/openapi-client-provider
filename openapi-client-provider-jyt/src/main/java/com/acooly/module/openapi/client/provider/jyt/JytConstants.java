/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.jyt;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class JytConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "jytProvider";
    public static final String PROVIDER_DEF_PRINCIPAL = "principal";

    public static final String SIGN = "sign";
    public static final String MERCHANT_NO = "merchant_id";
    public static final String XML_ENC = "xml_enc";
    public static final String KEY_ENC = "key_enc";

    public static final String NETBANK_NAME = "金运通网关";
}

package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.FreezeDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_FREEZE, type = ApiMessageType.Response)
public class QueryTransactionFreezeResponse extends BoscResponse {
	private List<FreezeDetailInfo> records;
	
	public List<FreezeDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<FreezeDetailInfo> records) {
		this.records = records;
	}
}
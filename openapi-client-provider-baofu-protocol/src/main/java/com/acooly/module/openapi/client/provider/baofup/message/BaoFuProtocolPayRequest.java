package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:56
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY,type = ApiMessageType.Request)
public class BaoFuProtocolPayRequest extends BaoFuPRequest {

    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字,未支付成功的订单号可重复提交
     */
    @NotBlank
    @Size(min = 8,max = 50)
    @BaoFuPAlias(value = "trans_id")
    private String transId;

    /**
     * 用户ID
     * 用户在商户平台唯一ID
     */
    @Size(max = 50)
    @BaoFuPAlias(value = "user_id")
    private String userId;

    /**
     * 签约协议号
     * 只有成功时该字段才有值
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @NotBlank
    @Size(max = 126)
    @BaoFuPAlias(value = "protocol_no",isEncrypt = true)
    private String protocolNo;

    /**
     * 交易金额
     * 单位：分
     * 例：1元则提交100
     */
    @NotBlank
    @Size(max = 12)
    @BaoFuPAlias(value = "txn_amt")
    private String amount;

    /**
     * 卡信息
     * 当使用信用卡支付时，需上传。
     * 格式：信用卡有效期|安全码(顺序不能变)
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 126)
    @BaoFuPAlias(value = "card_info",isEncrypt = true)
    private String cardInfo;

    /**
     * 风控参数
     * Json格式，详细参数见风控参数字段说明（通用参数、电商、互金消金、航旅、酒店、宝信、游戏、大宗）
     */
    @NotBlank
    @BaoFuPAlias(value = "risk_item")
    private String riskItem;

    /**
     * 交易成功通知地址
     * 最多填写三个地址
     * 不同的地址用‘|’连接
     */
    @Size(max = 500)
    @BaoFuPAlias(value = "return_url")
    private String notifyUrl;

}

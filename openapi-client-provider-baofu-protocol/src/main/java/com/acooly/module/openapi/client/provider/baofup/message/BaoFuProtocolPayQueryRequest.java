package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:56
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_QUERY,type = ApiMessageType.Request)
public class BaoFuProtocolPayQueryRequest extends BaoFuPRequest {

    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字,未支付成功的订单号可重复提交
     */
    @NotBlank
    @Size(min = 8,max = 50)
    @BaoFuPAlias(value = "orig_trans_id")
    private String origTransId;

    /**
     * 用户ID
     * 格式：yyyy-MM-dd HH:mm:ss如2017-12-19 20:19:19
     */
    @NotBlank
    @BaoFuPAlias(value = "orig_trade_date")
    private String origTransDate;

}

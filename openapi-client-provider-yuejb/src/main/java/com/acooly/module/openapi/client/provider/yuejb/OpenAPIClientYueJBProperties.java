package com.acooly.module.openapi.client.provider.yuejb;/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */

import com.acooly.core.utils.Strings;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.yuejb.OpenAPIClientYueJBProperties.PREFIX;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientYueJBProperties {
    public static final String PREFIX = "acooly.openapi.client.yuejb";
    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;
    /**
     * 网关地址
     */
    private String gateway;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 异步通知URL
     */
    private String notifyUrl;

    /**
     * 签名
     */
    private String sign;

    /**
     * 签名类型
     */
    private String signType;

    /**
     * 服务版本
     */
    private String version;

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

}

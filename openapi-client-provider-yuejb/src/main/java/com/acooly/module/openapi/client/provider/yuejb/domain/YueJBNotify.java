/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.yuejb.domain;

import lombok.Data;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBNotify extends YueJBResponse {
    /**
     * 通知时间
     */
    private String notifyTime;
}

/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.fuiou;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class FuiouConstants {

    public static final String SIGNATURE = "signature";
    /**
     * 签名实现KEY和秘钥
     */
    public static final String SIGNER_KEY = "fuiou";

}

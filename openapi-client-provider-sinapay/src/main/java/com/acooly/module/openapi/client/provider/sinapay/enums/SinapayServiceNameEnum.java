/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhike 2017-09-24 19:29
 */
public enum SinapayServiceNameEnum implements Messageable {

    UNKNOWN("UNKNOWN", "未知", SinapayApiServiceType.SYNC),
    CREATE_ACTIVATE_MEMBER("create_activate_member", "创建激活会员", SinapayApiServiceType.SYNC),
    SET_REAL_NAME("set_real_name", "设置实名信息", SinapayApiServiceType.SYNC),
    SET_PAY_PASSWORD("set_pay_password", "设置支付密码重定向", SinapayApiServiceType.SYNC),
    MODIFY_PAY_PASSWORD("modify_pay_password", "修改支付密码重定向", SinapayApiServiceType.SYNC),
    FIND_PAY_PASSWORD("find_pay_password", "找回支付密码重定向", SinapayApiServiceType.SYNC),
    QUERY_IS_SET_PAY_PASSWORD("query_is_set_pay_password", "查询是否设置支付密码", SinapayApiServiceType.SYNC),
    BINDING_BANK_CARD("binding_bank_card", "绑定银行卡", SinapayApiServiceType.SYNC),
    BINDING_BANK_CARD_ADVANCE("binding_bank_card_advance", "绑定银行卡推进", SinapayApiServiceType.SYNC),
    UNBINDING_BANK_CARD("unbinding_bank_card", "解绑银行卡", SinapayApiServiceType.SYNC),
    UNBINDING_BANK_CARD_ADVANCE("unbinding_bank_card_advance", "解绑银行卡推进", SinapayApiServiceType.SYNC),
    QUERY_BANK_CARD("query_bank_card", "查询银行卡", SinapayApiServiceType.SYNC),
    QUERY_BALANCE("query_balance", "查询余额/基金份额", SinapayApiServiceType.SYNC),
    QUERY_ACCOUNT_DETAILS("query_account_details", "查询收支明细", SinapayApiServiceType.SYNC),
    BALANCE_FREEZE("balance_freeze", "冻结余额", SinapayApiServiceType.SYNC),
    BALANCE_UNFREEZE("balance_unfreeze", "解冻余额", SinapayApiServiceType.SYNC),
    QUERY_CTRL_RESULT("query_ctrl_result", "查询冻结解冻结果", SinapayApiServiceType.SYNC),
    QUERY_MEMBER_INFOS("query_member_infos", "查询企业会员信息", SinapayApiServiceType.SYNC),
    QUERY_AUDIT_RESULT("query_audit_result", "查询企业会员审核结果", SinapayApiServiceType.SYNC),
    AUDIT_MEMBER_INFOS("audit_member_infos", "请求审核企业会员资质", SinapayApiServiceType.SYNC),
    SMT_FUND_AGENT_BUY("smt_fund_agent_buy", "经办人信息", SinapayApiServiceType.SYNC),
    QUERY_FUND_AGENT_BUY("query_fund_agent_buy", "查询经办人信息", SinapayApiServiceType.SYNC),
    SHOW_MEMBER_INFOS_SINA("show_member_infos_sina", "sina页面展示用户信息", SinapayApiServiceType.SYNC),
    MODIFY_VERIFY_MOBILE("modify_verify_mobile", "修改认证手机", SinapayApiServiceType.SYNC),
    FIND_VERIFY_MOBILE("find_verify_mobile", "找回认证手机", SinapayApiServiceType.SYNC),
    CHANGE_BANK_MOBILE("change_bank_mobile", "修改银行预留手机", SinapayApiServiceType.SYNC),
    CHANGE_BANK_MOBILE_ADVANCE("change_bank_mobile_advance", "修改银行预留手机推进", SinapayApiServiceType.SYNC),
    QUERY_TRADE_RELATED_NO("query_trade_related_no", "查询交易关联号下可代付金额", SinapayApiServiceType.SYNC),
    QUERY_FUND_QUOTA("query_fund_quota", "查询用户基金剩余额度", SinapayApiServiceType.SYNC),
    //订单类接口
    CREATE_HOSTING_COLLECT_TRADE("create_hosting_collect_trade", "创建代收交易", SinapayApiServiceType.ASYNC),
    CREATE_SINGLE_HOSTING_PAY_TRADE("create_single_hosting_pay_trade", "创建代付交易", SinapayApiServiceType.ASYNC),
    CREATE_BATCH_HOSTING_PAY_TRADE("create_batch_hosting_pay_trade", "创建批量代付交易", SinapayApiServiceType.ASYNC),
    PAY_HOSTING_TRADE("pay_hosting_trade", "交易支付", SinapayApiServiceType.SYNC),
    QUERY_PAY_RESULT("query_pay_result", "支付结果查询", SinapayApiServiceType.SYNC),
    QUERY_HOSTING_TRADE("query_hosting_trade", "交易查询", SinapayApiServiceType.SYNC),
    QUERY_HOSTING_BATCH_TRADE("query_hosting_batch_trade", "交易批次查询", SinapayApiServiceType.SYNC),
    CREATE_HOSTING_REFUND("create_hosting_refund", "退款", SinapayApiServiceType.SYNC),
    QUERY_HOSTING_REFUND("query_hosting_refund", "退款查询", SinapayApiServiceType.SYNC),
    CREATE_HOSTING_DEPOSIT("create_hosting_deposit", "充值", SinapayApiServiceType.ASYNC),
    QUERY_HOSTING_DEPOSIT("query_hosting_deposit", "充值查询", SinapayApiServiceType.SYNC),
    CREATE_HOSTING_WITHDRAW("create_hosting_withdraw", "提现", SinapayApiServiceType.ASYNC),
    QUERY_HOSTING_WITHDRAW("query_hosting_withdraw", "提现查询", SinapayApiServiceType.SYNC),
    CREATE_SINGLE_HOSTING_PAY_TO_CARD_TRADE("create_single_hosting_pay_to_card_trade", "创建单笔代付到提现卡交易", SinapayApiServiceType.ASYNC),
    CREATE_BATCH_HOSTING_PAY_TO_CARD_TRADE("create_batch_hosting_pay_to_card_trade", "创建批量代付到提现卡交易", SinapayApiServiceType.ASYNC),
    FINISH_PRE_AUTH_TRADE("finish_pre_auth_trade", "代收完成", SinapayApiServiceType.SYNC),
    CANCEL_PRE_AUTH_TRADE("cancel_pre_auth_trade", "代收撤销", SinapayApiServiceType.SYNC),
    CREATE_BID_INFO("create_bid_info", "标的录入", SinapayApiServiceType.SYNC),
    QUERY_BID_INFO("query_bid_info", "标的信息查询", SinapayApiServiceType.SYNC),
    //工具类接口
    QUERY_FUND_YIELD("query_fund_yield", "存钱罐基金收益率查询", SinapayApiServiceType.SYNC),
    //补充接口
    HANDLE_WITHHOLD_AUTHORITY("handle_withhold_authority", "委托扣款重定向", SinapayApiServiceType.SYNC),
    QUERY_WITHHOLD_AUTHORITY("query_withhold_authority", "查看用户是否委托扣款", SinapayApiServiceType.SYNC),
    QUERY_VERIFY("query_verify", "查询认证信息", SinapayApiServiceType.SYNC),
    BINDING_VERIFY("binding_verify", "绑定认证信息", SinapayApiServiceType.SYNC),
    UNBINDING_VERIFY("unbinding_verify", "解绑认证信息", SinapayApiServiceType.SYNC),
    ;

    private final String code;
    private final String message;
    private final SinapayApiServiceType apiServiceType;

    SinapayServiceNameEnum(String code, String message, SinapayApiServiceType apiServiceType) {
        this.code = code;
        this.message = message;
        this.apiServiceType = apiServiceType;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public SinapayApiServiceType getApiServiceType() {
        return apiServiceType;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (SinapayServiceNameEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static SinapayServiceNameEnum find(String code) {
        for (SinapayServiceNameEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<SinapayServiceNameEnum> getAll() {
        List<SinapayServiceNameEnum> list = new ArrayList<SinapayServiceNameEnum>();
        for (SinapayServiceNameEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (SinapayServiceNameEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}

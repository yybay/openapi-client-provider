/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayNotify;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.BatchPayItem;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_BATCH_HOSTING_PAY_TRADE, type = ApiMessageType.Notify)
public class CreateBatchHostingPayTradeNotify extends SinapayNotify {

	/**
	 * 商户网站唯一订单批次号或者交易原始批次凭证号
	 *
	 * 商户网站唯一订单批次号或者交易原始批次凭证号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "outer_batch_no")
	private String outerBatchNo;

	/**
	 * 内部交易订单批次号
	 *
	 * 新浪支付产生的交易订单批次号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "inner_batch_no")
	private String innerBatchNo;

	/**
	 * 批次总交易笔数
	 *
	 * 批次总交易笔数
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "batch_quantity")
	private String batchQuantity;

	/**
	 * 批次总金额
	 *
	 * 单位元，可以含小数点
	 */
	@NotEmpty
	@MoneyConstraint
	@ApiItem(value = "batch_amount")
	private Money batchAmount;

	/**
	 * 批次状态
	 *
	 * 批次处理状态， 详见附录中的批次状态
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "batch_status")
	private String batchStatus;

	/**
	 * 交易明细列表
	 *
	 * 交易明细内容详见“交易明细参数”，参数间用“~”分隔，各条目之间用“$”分隔
	 */
	@NotEmpty
	@ApiItem("trade_list")
	private List<BatchPayItem> tradeList;

	/**
	 * 批次创建时间
	 *
	 * 批次创建时间，格式yyyyMMddHHmmss
	 */
	@NotEmpty
	@Size(max = 14)
	@ApiItem(value = "gmt_create")
	private String gmtCreate;

	/**
	 * 批次处理结束时间
	 *
	 * 批次处理结束时间，格式yyyyMMddHHmmss
	 */
	@Size(max = 14)
	@ApiItem(value = "gmt_finished")
	private String gmtFinished;
}

package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.FIND_VERIFY_MOBILE, type = ApiMessageType.Request)
public class FindVerifyMobileRequest extends SinapayRequest {

    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *收银台地址类别
     * 收银台地址类型，目前只包含MOBILE。为空时默认返回PC版页面，当传值为“MOBILE”时返回移动版页面。
     */
    @Size(max = 10)
    @ApiItem(value = "cashdesk_addr_category")
    private String cashdeskAddrCategory;
}

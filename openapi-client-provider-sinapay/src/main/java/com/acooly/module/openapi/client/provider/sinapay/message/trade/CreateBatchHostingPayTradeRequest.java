/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayOutTradeCode;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradePayInfo;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_BATCH_HOSTING_PAY_TRADE, type = ApiMessageType.Request)
public class CreateBatchHostingPayTradeRequest extends SinapayRequest {

	public static String Notify_METHOD_SINGLE = "single_notify";
	public static String Notify_METHOD_BATCH = "batch_notify";
	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_pay_no")
	private String outPayNo = Ids.oid();

	/**
	 * 交易码
	 *
	 * 商户网站代收交易业务码，见附录
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "out_trade_code")
	private String outTradeCode = SinapayOutTradeCode.INVEST_OUT.code();

	/**
	 * 通知方式
	 *
	 * 取值范围：single_notify, batch_notify
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "notify_method")
	private String notifyMethod = Notify_METHOD_BATCH;

	/**
	 * 交易列表
	 */
	@NotEmpty
	@ApiItem(value = "trade_list")
	private List<TradePayInfo> tradePayInfos = Lists.newArrayList();

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "user_ip")
	private String userIp;
}

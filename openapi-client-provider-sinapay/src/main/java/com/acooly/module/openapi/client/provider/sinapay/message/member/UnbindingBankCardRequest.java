/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.UNBINDING_BANK_CARD, type = ApiMessageType.Request)
public class UnbindingBankCardRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 绑卡ID */
	@Size(max = 32)
	@NotEmpty
	@ApiItem(value = "card_id")
	private String cardId;

	/** 是否推进方式（必须为Y） */
	@Size(max = 1)
	@NotEmpty
	@ApiItem(value = "advance_flag")
	private String advanceFlag = "Y";

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;
}

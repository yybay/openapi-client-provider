/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@SinapayApiMsg(service = SinapayServiceNameEnum.BINDING_BANK_CARD_ADVANCE, type = ApiMessageType.Request)
@Getter
@Setter
public class BindingBankCardAdvanceRequest extends SinapayRequest {

	/** 绑卡请求号 */
	@Size(max = 100)
	@NotEmpty
	private String ticket;

	/** 短信验证码 */
	@Size(max = 32)
	@NotEmpty
	@ApiItem("valid_code")
	private String validCode;

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;

}

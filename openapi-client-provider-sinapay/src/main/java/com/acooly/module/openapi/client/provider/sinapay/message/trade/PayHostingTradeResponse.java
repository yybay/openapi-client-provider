package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 交易支付
 *
 * @author xiaohong
 * @create 2018-07-17 9:55
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.PAY_HOSTING_TRADE, type = ApiMessageType.Response)
public class PayHostingTradeResponse extends SinapayResponse {

    /**
     * 支付订单号
     *
     * 商户网站交易订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_pay_no")
    private String outPayNo;

    /**
     * 支付状态
     */
    @Size(max = 16)
    @ApiItem(value = "pay_status")
    private String payStatus;

    /**
     * 后续推进需要的参数
     *
     * 如果支付需要推进则会返回此参数，
     * 支付推进时需要带上此参数，ticket有效期为15分钟，可以多次使用（最多5次）
     */
    @Size(max = 100)
    @ApiItem(value = "ticket")
    private String ticket;

    /**
     * 收银台重定向地址
     * 当请求参数中的“version”的值是“1.1”时，且支付方式扩展是网银并选择“SINAPAY”跳转新浪收银台时，此参数不为空。商户系统需要将用户按此参数的值重定向到新浪收银台。其他情况不返回此值，“version”的值是“1.0”时也不返回此值。
     */
    @Size(max = 200)
    @ApiItem(value = "redirect_url")
    private String redirectUrl;
}

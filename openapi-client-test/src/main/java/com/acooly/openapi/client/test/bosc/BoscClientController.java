/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 17:56 创建
 */
package com.acooly.openapi.client.test.bosc;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.Servlets;
import com.acooly.module.openapi.client.provider.bosc.BoscApiService;
import com.acooly.module.openapi.client.provider.bosc.BoscFundApiService;
import com.acooly.module.openapi.client.provider.bosc.BoscMemberApiService;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscExpectPayCompanyEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeWayEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscIdCardTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.RechargeRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.WithdrawRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.EnterpriseRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalRegisterNotify;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.UnbindBankcardRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 上海银行P2P存管 控制层Demo和test
 *
 * @author zhangpu 2017-09-25 17:56
 */
@Slf4j
@Controller
@RequestMapping("/openapi/client/bosc")
public class BoscClientController {
	
	@Autowired
	private BoscApiService boscApiService;
	
	@Autowired
	private BoscFundApiService fundApiService;
	
	private final String redirectUrl = "http://localhost:8080/openapi/client/bosc/commonReturn.html";
	
	private final String investorPlatformUserNo = "liubinttest";
	
	private final String borrowerPlatformUserNo = "liubin_b_1";
	
	private final String redirectJumpPager = "/bosc/personalRegister";
	
	@Autowired
	private BoscMemberApiService memberApiService;
	
	
	/**
	 * 注册: 跳转银行
	 *
	 * @param request
	 * @param model
	 *
	 * @return
	 */
	@RequestMapping("personalRegister")
	public Object personalRegister (HttpServletRequest request, Model model, BoscUserRoleEnum userRole,
	                                String platformUserNo) {
		String redirectUrl = "http://localhost:8080/openapi/client/bosc/personalRegisterReturn.html";
		PersonalRegisterRequest req = new PersonalRegisterRequest (platformUserNo, userRole, redirectUrl);
		String formHtml = boscApiService.personalRegisterRedirect (req);
		model.addAttribute ("formHtml", formHtml);
		log.info (formHtml);
		return "/bosc/personalRegister";
	}
	
	
	@RequestMapping("enterpriseRegister")
	public String enterpriseRegister (Model model) {
		String redirectUrl = "http://localhost:8080/openapi/client/bosc/commonReturn.html";
		EnterpriseRegisterRequest registerRequest = new EnterpriseRegisterRequest ("liubinenterprisetest12",
		                                                                           BoscUserRoleEnum.BORROWERS,
		                                                                           redirectUrl,
		                                                                           Ids.oid ());
		
		registerRequest.setEnterpriseName ("刘斌的测试企业");
		registerRequest.setBankLicense ("324234234234234");
		registerRequest.setContact ("刘斌");
		registerRequest.setContactPhone ("15310302258");
		registerRequest.setUnifiedCode ("3333333333");
		registerRequest.setIdCardType (BoscIdCardTypeEnum.PRC_ID);
		registerRequest.setLegal ("刘斌");
		registerRequest.setLegalIdCardNo ("411082198904052477");
		registerRequest.setBankcardNo ("enterpriseRegister");
		registerRequest.setBankcode (BoscBankcodeEnum.ABOC);
		String formHtml = boscApiService.enterpriseRegisterRedirect (registerRequest);
		model.addAttribute ("formHtml", formHtml);
		return "/bosc/personalRegister";
	}
	
	/**
	 * 注册：银行return
	 * (测试后，貌似没有异步通知，只有同步)
	 *
	 * @param request
	 *
	 * @return
	 */
	@RequestMapping("personalRegisterReturn")
	@ResponseBody
	public Object personalRegisterReturn (HttpServletRequest request) {
		Map<String, String> data = Servlets.getParameters (request, null, false);
		PersonalRegisterNotify notify = boscApiService.personalRegisterNotice (data);
		return notify;
	}
	
	
	@RequestMapping("commonReturn")
	@ResponseBody
	public Object commonReturn (HttpServletRequest request) {
		return boscApiService.commonNotifyParser (Servlets.getParameters (request, null, false));
	}
	
	@RequestMapping("withdraw")
	public String withdraw (Model model) {
		WithdrawRequest request = new WithdrawRequest (investorPlatformUserNo, Ids.oid (),
		                                               Dates.addDate (new Date (), 3 * 60 * 1000), redirectUrl,
		                                               new Money (5000),
		                                               null);
		model.addAttribute ("formHtml", fundApiService.withdrawRedirect (request));
		return redirectJumpPager;
	}
	
	
	@RequestMapping("recharge")
	public String recharge (Model model, BoscRechargeWayEnum rechargeWayEnum) {
		RechargeRequest request = new RechargeRequest (investorPlatformUserNo, Ids.oid (), new Money (5000), null,
		                                               rechargeWayEnum, redirectUrl,
		                                               Dates.addDate (new Date (), 3 * 60 * 1000));
		request.setExpectPayCompany (BoscExpectPayCompanyEnum.YEEPAY);
		request.setBankcode (BoscBankcodeEnum.ABOC);
		model.addAttribute ("formHtml", fundApiService.rechargeRedirect (request));
		
		return redirectJumpPager;
	}
	
	@RequestMapping("unbindBankCard")
	public String unbindBankCard(Model model){
		UnbindBankcardRequest request = new UnbindBankcardRequest ("201702050677", Ids.getDid (), redirectUrl);
		model.addAttribute ("formHtml",memberApiService.unbindBankcard (request));
		return redirectJumpPager;
	}
}

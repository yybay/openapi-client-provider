package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.NETBANK_DEPOSIT, type = ApiMessageType.Response)
public class YinShengEbankDepositResponse extends YinShengResponse{

    /**
     * 响应form表单
     */
    private String htmlForm;
}

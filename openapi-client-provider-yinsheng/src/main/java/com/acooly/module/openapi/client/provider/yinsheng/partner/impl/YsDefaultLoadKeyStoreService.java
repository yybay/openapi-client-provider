package com.acooly.module.openapi.client.provider.yinsheng.partner.impl;

import com.acooly.core.utils.security.RSA;
import com.acooly.module.openapi.client.provider.yinsheng.OpenAPIClientYinShengProperties;
import com.acooly.module.openapi.client.provider.yinsheng.YinShengConstants;
import com.acooly.module.safety.key.AbstractKeyLoadManager;
import com.acooly.module.safety.key.KeyStoreLoader;
import com.acooly.module.safety.support.KeyStoreInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class YsDefaultLoadKeyStoreService extends AbstractKeyLoadManager<KeyStoreInfo> implements KeyStoreLoader {

    @Autowired
    private OpenAPIClientYinShengProperties openAPIClientYinShengProperties;

    @Override
    public KeyStoreInfo doLoad(String principal) {
        KeyStoreInfo keyStoreInfo = new KeyStoreInfo();
        keyStoreInfo.setCertificateUri(openAPIClientYinShengProperties.getPublicKeyPath());
        keyStoreInfo.setKeyStoreUri(openAPIClientYinShengProperties.getPrivateKeyPath());
        keyStoreInfo.setKeyStorePassword(openAPIClientYinShengProperties.getPrivateKeyPassword());
        keyStoreInfo.setPlainEncode("UTF-8");
        keyStoreInfo.setKeyStoreType(RSA.KEY_STORE_PKCS12);
        keyStoreInfo.loadKeys();
        return keyStoreInfo;
    }

    @Override
    public String getProvider() {
        return YinShengConstants.PROVIDER_NAME;
    }
}

/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.yinsheng.partner.impl;

import com.acooly.module.openapi.client.provider.yinsheng.OpenAPIClientYinShengProperties;
import com.acooly.module.openapi.client.provider.yinsheng.partner.YinShengPartnerIdLoadManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("yinShengPartnerIdLoadManager")
public class YinShengEmptyPartnerIdLoader implements YinShengPartnerIdLoadManager {

    @Autowired
    private OpenAPIClientYinShengProperties openAPIClientYinShengProperties;

    @Override
    public String load(String orderNo) {
        return openAPIClientYinShengProperties.getPartnerId();
    }
}

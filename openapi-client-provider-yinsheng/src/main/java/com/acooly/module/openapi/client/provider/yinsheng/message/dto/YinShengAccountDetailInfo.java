package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/2/22 11:18
 */
@Getter
@Setter
public class YinShengAccountDetailInfo implements Serializable{

    /**
     * 账户号
     */
    private String account_id;

    /**
     * 账户类型
     */
    private String account_type;

    /**
     * 账户余额
     */
    private String account_amount;
}

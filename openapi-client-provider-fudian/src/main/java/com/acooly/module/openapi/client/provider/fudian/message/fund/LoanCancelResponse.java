/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_CANCEL ,type = ApiMessageType.Response)
public class LoanCancelResponse extends FudianResponse {

    /**
     * 发标的订单日期
     * 发标的时候的订单流水日期
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String loanOrderDate;

    /**
     * 发标的订单流水号
     * 发标的时候的订单流水号
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String loanOrderNo;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 标的状态
     * 标的状态：0：开标、1：投标中、2：还款中、3：已还款、4：结束、5：撤标
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;
}
package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/30 17:43
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BILL_DOWNLOAD,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuBillDowloadResponse extends BaoFuResponse{
    /**
     * 返回文件体
     */
    @XStreamAlias("resp_body")
    private String respBody;

    /**
     * 预留域
     */
    @XStreamAlias("reserved")
    private String reserved;

}

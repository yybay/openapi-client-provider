package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/2/23 15:29
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.REFUND_QUERY,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuRefundQueryResponse extends BaoFuResponse{

    /**
     * 请求方保留域
     */
    @XStreamAlias("req_reserved")
    private String reqReserved;

    /**
     * 附加字段
     */
    @XStreamAlias("additional_info")
    private String additionalInfo;

    /**
     * 退款商户订单号
     */
    @XStreamAlias("refund_order_no")
    private String refundOrderNo;

    /**
     * 成功退款金额
     */
    @XStreamAlias("refund_amt")
    private String refundAmt;
}

package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author zhike 2018/2/2 11:53
 */
@Getter
@Setter
@XStreamAlias("trans_reqData")
public class BaoFuWithdrawQueryRequestInfo implements Serializable{
    /**
     * 商户订单号
     * 循环域
     * 商户唯一流水号，
     * 具体格式建议：商户编号+14位日期+3位随机数
     */
    @NotBlank
    @XStreamAlias("trans_no")
    private String transNo;

    /**
     *宝付批次号  宝付代付交易返回批次号
     */
    @XStreamAlias("trans_batchid")
    private String transBatchid;

}

package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/2/23 15:29
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.REFUND_QUERY,type = ApiMessageType.Request)
public class BaoFuRefundQueryRequest extends BaoFuRequest{
    /**
     * 退款商户订单号 退款时商户端生成的订单号
     */
    @NotBlank
    @BaoFuAlias(value = "refund_order_no", request = false)
    private String refundOrderNo;

    /**
     * 商户流水号 每次发起退款不能重复
     */
    @NotBlank
    @BaoFuAlias(value = "trans_serial_no", request = false)
    private String transSerialNo;

    /**
     * 附加字段
     */
    @BaoFuAlias(value = "additional_info")
    @Size(max = 128)
    private String additionalInfo;

    /**
     * 请求方保留域
     */
    @BaoFuAlias(value = "req_reserved")
    private String reqReserved;

}

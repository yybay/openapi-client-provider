package com.acooly.module.openapi.client.provider.baofu.marshall.info;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/2/8 10:53
 */
@Getter
@Setter
public class ResponseInfo implements Serializable{

    private String message;

    private String partnerId;
}

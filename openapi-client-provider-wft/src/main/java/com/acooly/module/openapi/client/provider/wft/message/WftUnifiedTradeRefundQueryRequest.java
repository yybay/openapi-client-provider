package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/12/7 10:25
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_REFUNDQUERY, type = ApiMessageType.Request)
public class WftUnifiedTradeRefundQueryRequest extends WftRequest {
    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    @Size(max = 32)
    private String transactionId;

    /**
     * 商户退款单号
     */
    @WftAlias(value = "out_refund_no")
    @Size(max = 32)
    private String outRefundNo;

    /**
     * 平台退款单号
     * 平台退款单号关于refund_id、out_refund_no、out_trade_no 、transaction_id 四个参数必填一个， 如果同时存在优先级为：refund_id>out_refund_no>transaction_id>out_trade_no特殊说明：如果是支付宝，refund_id、out_refund_no必填其中一个
     */
    @WftAlias(value = "refund_id")
    @Size(max = 32)
    private String refundId;


    /**
     * 订单号
     */
    @WftAlias(value = "nonce_str")
    @NotBlank(message = "随机字符串不能为空")
    @Size(max = 32)
    private String nonceStr;


    @Override
    public void doCheck() {
        if (Strings.isBlank(refundId) && Strings.isBlank(outRefundNo)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "平台退款订单号和商户退款订单号不能同时为空");
        }
    }
}

package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradePrePayResponseBody implements Serializable {

	/**
	 * 返回码组件。当ResultStatus=S时才有后续的参数返回。
	 */
	@XStreamAlias("RespInfo")
	private WsbankResponseInfo responseInfo;

	/**
	 * 网商支付订单号。明确支付成功一定有
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;

//	/**
//	 * 第三方支付授权码。扫码支付授权码，设备读取用户支付宝或微信中的条码或者二维码信息
//	 */
//	@Size(max = 64)
//	@XStreamAlias("AuthCode")
//	@NotBlank
//	private String authCode;

	/**
	 * 外部交易号。由合作方系统生成，只能包含字母、数字、下划线；需保证合作方系统不重复。
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;
	
	/**
	 * 原生态js支付信息。仅微信，用于H5唤醒微信App完成支付。
	 */
	@Size(max = 512)
	@XStreamAlias("PayInfo")
	@NotBlank
	private String payInfo;
	
	/**
	 * 第三方支付返回的预付单信息。备注：若使用支付宝交易，本字段返回的值回填至JSAPI中的tradeNo字段
	 */
	@Size(max = 64)
	@XStreamAlias("PrePayId")
	@NotBlank
	private String prePayId;

}

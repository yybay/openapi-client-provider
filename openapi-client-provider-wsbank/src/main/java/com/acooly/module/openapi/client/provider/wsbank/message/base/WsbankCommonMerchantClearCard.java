package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsbankCommonMerchantClearCard implements Serializable {

	/**
	 * 银行卡号
	 */
	@NotBlank
	@JSONField(name="BankCardNo")
	@Size(max = 32)
	private String bankCardNo;

	/**
	 * 银行账户户名
	 */
	@NotBlank
	@Size(max = 256)
	@JSONField(name="BankCertName")
	private String bankCertName;

	/**
	 * 账户类型。可选值： 01：对私账户 02：对公账户
	 */
	@NotBlank
	@Size(max = 8)
	@JSONField(name="AccountType")
	private String accountType;

	/**
	 * 联行号
	 */
	@Size(max =64)
	@JSONField(name="ContactLine")
	private String contactLine;

	/**
	 * 开户支行名称
	 */
	@Size(max = 128)
	@JSONField(name="BranchName")
	private String branchName;

	/**
	 * 开户支行所在省
	 */
	@Size(max = 16)
	@JSONField(name="BranchProvince")
	private String branchProvince;

	/**
	 * 开户支行所在市
	 */
	@Size(max = 16)
	@JSONField(name="BranchCity")
	private String branchCity;

	/**
	 * 持卡人证件类型。可选值： 01：身份证
	 */
	@Size(max = 8)
	@JSONField(name="CertType")
	private String certType;

	/**
	 * 持卡人证件号码
	 */
	@Size(max = 32)
	@JSONField(name="CertNo")
	private String certNo;

	/**
	 * 持卡人地址
	 */
	@Size(max = 128)
	@JSONField(name="CardHolderAddress")
	private String cardHolderAddress;

	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}

package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankAlipayResponseBody implements Serializable {

	private static final long serialVersionUID = -88986219669638671L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 网商银行订单号
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;

	/**
	 * 支付渠道商户订单号
	 */
	@XStreamAlias("MerchantOrderNo")
	private String merchantOrderNo;

	/**
	 * 外部订单号
	 */
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;

	/**
	 * 支付宝分配给开发者的应用ID
	 */
	@XStreamAlias("OutAppId")
	private String outAppId;

	/**
	 * 接口名称
	 */
	@XStreamAlias("Method")
	private String method;

	/**
	 * 仅支持JSON
	 */
	@XStreamAlias("Format")
	private String format;

	/**
	 * 请求使用的编码格式，如utf-8,gbk,gb2312等
	 */
	@XStreamAlias("Charset")
	private String charset;

	/**
	 * 商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
	 */
	@XStreamAlias("SignType")
	private String signType;

	/**
	 * 商户请求参数的签名串，详见签名
	 */
	@XStreamAlias("Sign")
	private String sign;

	/**
	 * 发送请求的时间，格式"yyyy-MM-dd HH:mm:ss"
	 */
	@XStreamAlias("Timestamp")
	private String timestamp;

	/**
	 * 调用的接口版本，固定为：1.0
	 */
	@XStreamAlias("Version")
	private String version;

	/**
	 * HTTP/HTTPS开头字符串，WAP支付时返回地址
	 */
	@XStreamAlias("ReturnUrl")
	private String returnUrl;

	/**
	 * 支付宝服务器主动通知商户服务器里指定的页面http/https路径。建议商户使用https
	 */
	@XStreamAlias("NotifyUrl")
	private String notifyUrl;

	/**
	 * 业务请求参数的集合，最大长度不限，除公共参数外所有请求参数都必须放在这个参数中传递，具体参照各产品快速接入文档
	 */
	@XStreamAlias("BizContent")
	private String bizContent;

}

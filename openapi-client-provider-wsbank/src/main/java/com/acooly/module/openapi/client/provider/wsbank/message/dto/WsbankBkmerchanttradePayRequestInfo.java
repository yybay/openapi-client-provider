package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
@XStreamAlias("request")
public class WsbankBkmerchanttradePayRequestInfo implements Serializable {

    /**
     * 请求报文头
     */
    @XStreamAlias("head")
    @NotNull
    private WsbankHeadRequest headRequest;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    @NotNull
    private WsbankBkmerchanttradePayRequestBody requestBody;
}

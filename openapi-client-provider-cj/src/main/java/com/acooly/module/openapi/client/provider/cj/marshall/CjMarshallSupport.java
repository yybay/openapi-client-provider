/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月4日
 *
 */
package com.acooly.module.openapi.client.provider.cj.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.cj.CjConstants;
import com.acooly.module.openapi.client.provider.cj.CjProperties;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMessage;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchQueryRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealQueryRequest;
import com.acooly.module.openapi.client.provider.cj.utils.CjSignHelper;
import com.acooly.module.openapi.client.provider.cj.utils.XmlHelper;
import com.acooly.module.safety.key.KeyLoadManager;
import com.acooly.module.safety.signature.SignerFactory;
import com.acooly.module.safety.support.KeyStoreInfo;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

/**
 * @author
 */
@Slf4j
public class CjMarshallSupport {

    @Autowired
    protected CjProperties cjProperties;

    @Autowired
    protected SignerFactory signerFactory;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;

    protected CjProperties getProperties() {
        return cjProperties;
    }

    protected String doMarshall(CjRequest source) {
        doVerifyParam(source);
        String reqMsg ="";
        //组装报文
        if(Strings.equals(CjServiceEnum.CJ_REAL_DEDUCT.getKey(),source.getTrxCode())){
            CjRealDeductRequest request = (CjRealDeductRequest)source;
            reqMsg = XmlHelper.buildCjRealMsg(request);
        }else if(Strings.equals(CjServiceEnum.CJ_BATCH_DEDUCT.getKey(),source.getTrxCode())){
            CjBatchDeductRequest request = (CjBatchDeductRequest)source;
            reqMsg = XmlHelper.buildCjBatchMsg(request);
        }else if(Strings.equals(CjServiceEnum.CJ_REAL_DEDUCT_QUERY.getKey(),source.getTrxCode())){
            CjRealQueryRequest request = (CjRealQueryRequest)source;
            reqMsg = XmlHelper.buildRealQueryMsg(request);
        }else if(Strings.equals(CjServiceEnum.CJ_BATCH_DEDUCT_QUERY.getKey(),source.getTrxCode())){
            CjBatchQueryRequest request = (CjBatchQueryRequest)source;
            reqMsg = XmlHelper.buildBatchQueryMsg(request);
        }
        log.debug("待签字符串:{}", reqMsg);
        return doSign(reqMsg, source);
    }


    /**
     * 校验参数
     *
     * @param source
     */
    protected void doVerifyParam(CjApiMessage source) {
        try {
            source.doCheck();
            Validators.assertJSR303(source);
        } catch (Exception e) {
            throw new ApiClientException(e.getMessage());
        }
    }

    protected void beforeMarshall(CjApiMessage message) {
        if(Strings.isBlank(message.getPartnerId())) {
            message.setPartnerId(getProperties().getMertid());
        }
    }

    /**
     * 签名 优先获取传进来的，如果传进来没有则用配置文件中的
     *
     * @param waitForSign
     * @param source
     * @return
     */
    protected String doSign(String waitForSign, CjRequest source) {
        KeyStoreInfo keyStoreInfo = getKeyStoreInfo(source);
        CjSignHelper singHelper = new CjSignHelper(keyStoreInfo);
        return singHelper.signXml$Add(waitForSign);
    }

    /**
     * 获取keyStoreInfo
     *
     * @return
     */
    protected KeyStoreInfo getKeyStoreInfo(CjRequest source) {
        KeyStoreInfo keyStoreInfo = keyStoreLoadManager.load(source.getPartnerId(), CjConstants.PROVIDER_NAME);
        return keyStoreInfo;
    }

}

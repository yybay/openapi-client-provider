package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayBankCardTypeEnum;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayCardTypeEnum;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/5/17 18:55
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.PRECHECK_FOR_SIGN,type = ApiMessageType.Request)
public class ShengpayPrecheckForSignRequest extends ShengpayRequest {

    /**
     * 签约请求号，必须唯一（商户系统生成的唯一签约请求号）
     */
    @NotBlank
    @Size(max = 32)
    private String requestNo;

    /**
     * 是否重发短信。重发短信时，其它参数仍然需要提交，会校验一致性，且requestNo保持不变
     */
    @NotBlank
    private String isResendValidateCode = "false";

    /**
     * 商户系统内针对会员的唯一标识，不同会员的标识必须唯一
     */
    @NotBlank
    @Size(max = 32)
    private String outMemberId;

    /**
     * 机构代码，如：ICBC
     * 参考枚举：com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayBankCodeEnum
     */
    @NotBlank
    private String bankCode;

    /**
     * 银行卡类型，如：CR/DR
     */
    @NotBlank
    private String bankCardType = ShengpayBankCardTypeEnum.BANK_CARD_TYPE_DR.getCode();

    /**
     * 银行卡明文
     */
    @NotBlank
    private String bankCardNo;

    /**
     * 用户真实姓名，应与银行卡账户姓名保持一致
     */
    @NotBlank
    private String realName;

    /**
     * 证件号码, 与银行预留一致
     */
    @NotBlank
    private String idNo;

    /**
     * 证件类型，如：IC
     */
    @NotBlank
    private String idType = ShengpayCardTypeEnum.CARD_TYPE_IC.getCode();

    /**
     * 银行预留手机号
     */
    @NotBlank
    private String mobileNo;

    /**
     * 信用卡CVV2, 信用卡首次支付时该字段必填
     */
    private String cvv2;

    /**
     * 信用卡有效期，格式：yy/mm， 信用卡首次支付时必填
     */
    private String validThru;

    /**
     * 用户IP（用户支付时的IP）
     */
    @NotBlank
    private String userIp;
}

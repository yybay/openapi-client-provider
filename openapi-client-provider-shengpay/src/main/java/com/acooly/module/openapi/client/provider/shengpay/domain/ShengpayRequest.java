/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.domain;

import com.acooly.core.utils.Dates;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * @author zhike 2018-01-23 12:58
 */
@Getter
@Setter
public class ShengpayRequest extends ShengpayMessage {

    /**
     * 请求时间: yyyyMMddHHmmss如:20110707112233
     */
    private String requestTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     * 异步回调地址
     * 服务器通知业务参数，请根据此做业务处理
     */
    @Length(max = 256)
    private String notifyUrl;

    /**
     * 会跳地址
     */
    private String returnUrl;
}

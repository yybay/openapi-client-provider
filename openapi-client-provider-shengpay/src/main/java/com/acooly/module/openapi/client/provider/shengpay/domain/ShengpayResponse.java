/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018-01-23 12:58
 * <p>
 */
@Getter
@Setter
public class ShengpayResponse extends ShengpayMessage {

    /**
     * 返回码
     */
    private String returnCode;

    /**
     * 返回消息
     */
    private String returnMessage;

}
